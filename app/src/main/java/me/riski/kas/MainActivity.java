package me.riski.kas;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.util.Log;
import android.view.View;

import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

import me.riski.kas.helpers.SQLiteHelper;

public class MainActivity extends AppCompatActivity {

    SQLiteHelper sqLiteHelper;
    Cursor cursor;
    String transactionId;

    ListView listViewKas;
    TextView textTotalOut, textTotalIn, textBalance;
    ArrayList<HashMap<String, String>> arusKas;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab_add);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                navigateToAddActivity();
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
            }
        });


        textTotalOut    = findViewById(R.id.text_total_out);
        textTotalIn     = findViewById(R.id.text_total_in);
        textBalance     = findViewById(R.id.text_balance);
        listViewKas     = findViewById(R.id.list_view_kas);
        arusKas         = new ArrayList<>();

        // SQLite
        sqLiteHelper = new SQLiteHelper(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        kasAdapter();
    }

    private void getTotal() {
        NumberFormat rupiah = NumberFormat.getInstance(Locale.US);
        SQLiteDatabase database = sqLiteHelper.getReadableDatabase();
        cursor = database.rawQuery("SELECT SUM(nominal) AS total," +
                "(SELECT SUM(nominal) FROM transactions WHERE status='MASUK') AS MASUK, " +
                "(SELECT SUM(nominal) FROM transactions WHERE status='KELUAR') AS KELUAR FROM transactions", null);
        cursor.moveToFirst();

        textTotalIn.setText( rupiah.format(cursor.getDouble(1)) );
        textTotalOut.setText( rupiah.format(cursor.getDouble(2)) );
        textBalance.setText( rupiah.format(cursor.getDouble(1) - cursor.getDouble(2)) );
    }


    private void kasAdapter() {
        arusKas.clear();
        listViewKas.setAdapter(null);

        SQLiteDatabase database = sqLiteHelper.getReadableDatabase();

        cursor = database.rawQuery("SELECT *, strftime('%d/%m/%Y', created_at) FROM transactions ORDER BY id DESC", null);
        cursor.moveToFirst();

        for (int i = 0; i < cursor.getCount(); i++) {
            cursor.moveToPosition(i);

            HashMap<String, String> map = new HashMap<>();
            map.put("id", cursor.getString(0));
            map.put("status", cursor.getString(1));
            map.put("nominal", "Rp." + cursor.getString(2));
            map.put("notes", cursor.getString(3));
            map.put("created_at", cursor.getString(5));

            arusKas.add(map);

//            Log.e("*** nominal", cursor.getString(2));
//            Log.e("*** notes", cursor.getString(3));
        }

        SimpleAdapter simpleAdapter = new SimpleAdapter(
                this,
                arusKas,
                R.layout.list_item_kas,
                new String[] {"id", "nominal", "status", "notes", "created_at"},
                new int[] { R.id.list_item_id, R.id.text_nominal,  R.id.text_status, R.id.text_notes, R.id.text_created_at}
                );

        listViewKas.setAdapter(simpleAdapter);
        listViewKas.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                transactionId = ((TextView)view.findViewById(R.id.list_item_id)).getText().toString();
                Log.e("*** dialog ***", "Dialog appear");
                showDialogOptions();
            }
        });

        getTotal();
    }

    private void showDialogOptions() {
        Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.dialog_options);
        dialog.getWindow().setLayout(
                WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.WRAP_CONTENT
        );

        TextView textDelete = dialog.findViewById(R.id.text_delete);
        TextView textEdit = dialog.findViewById(R.id.text_edit);

        textDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                handleDeleteModal();
            }
        });

        textEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("*** dialog ***", "Edit button clicked");
            }
        });

        dialog.show();
    }

    private void handleDeleteModal() {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle("Ingin hapus item ini ?");
        alert.setMessage("Data yang dihapus tidak dapat dikembalikan!");

        // Positive button "Yes"
        alert.setPositiveButton(
                "Yes",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        deleteItemFromDatabase(transactionId);
                    }
                }
        );

        // Negative button "No"
        alert.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        Log.e("*** dialog ***", "Delete button clicked");
        alert.show();
    }

    private void deleteItemFromDatabase(String id) {
        // Delete data from database
        SQLiteDatabase database = sqLiteHelper.getWritableDatabase();
        database.execSQL("DELETE FROM transactions WHERE id=' " + id + " ' ");
        Toast.makeText(getApplicationContext(), "Data berhasil dihapus!", Toast.LENGTH_SHORT).show();

        // Re calculate kas adapter
        kasAdapter();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void navigateToAddActivity() {
        Intent intent = new Intent(this, AddActivity.class);
        startActivity(intent);
    }
}