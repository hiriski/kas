package me.riski.kas;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import me.riski.kas.helpers.SQLiteHelper;

public class AddActivity extends AppCompatActivity {

    SQLiteHelper sqliteHelper;

    EditText editTextNominal, editTextNotes;
    RadioGroup radioType;
    RadioButton radioTypeOut, radioTypeIn;
    Button buttonSave;

    String databaseName;
    CharSequence toastText;
    String inputStatus;
    Integer toastDuration = Toast.LENGTH_SHORT;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);

        getSupportActionBar().setTitle("Tambah Catatan Kas");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        sqliteHelper = new SQLiteHelper(this);

        databaseName = "transactions";
        toastText = "";
        inputStatus = "";

        radioType       = findViewById(R.id.radio_type);
        radioTypeOut    = findViewById(R.id.radio_type_out);
        radioTypeIn     = findViewById(R.id.radio_type_in);
        editTextNominal = findViewById(R.id.edit_text_nominal);
        editTextNotes   = findViewById(R.id.edit_text_notes);
        buttonSave      = findViewById(R.id.button_save);

        radioType.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch(checkedId) {
                    case R.id.radio_type_in:
                        inputStatus = "MASUK";
                        break;
                    case R.id.radio_type_out:
                        inputStatus = "KELUAR";
                        break;
                }
            }
        });



        buttonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Context context = getApplicationContext();

                Log.d("_foo", "bar");

                if(isEmpty()) {
                    toastText = "Isi data dengan benar.";
                    editTextNominal.requestFocus();
                } else if(inputStatus.equals("")) {
                    toastText  = "Pilih status";
                    radioType.requestFocus();
                } else if(editTextNominal.getText().toString().equals("")) {
                    toastText  = "Isi nominal";
                    editTextNominal.requestFocus();
                } else if(editTextNotes.getText().toString().equals("")) {
                    toastText  = "Isi keterangan";
                    editTextNotes.requestFocus();
                } else {
                    saveDataToTheDatabase();
                }

                Toast toast = Toast.makeText(context, toastText, toastDuration);
                toast.show();
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }

    private boolean isEmpty() {
        if(editTextNominal.getText().toString().equals("") && editTextNotes.getText().toString().equals("") && inputStatus.equals("")) {
            return  true;
        }
        return false;
    }

    private void saveDataToTheDatabase() {
        SQLiteDatabase database  = sqliteHelper.getWritableDatabase();
        database.execSQL("INSERT INTO transactions (status, nominal, notes) VALUES ('"+ inputStatus +"', '" + editTextNominal.getText().toString() +"', '" + editTextNotes.getText().toString() + "')");
        Log.d("_db", String.valueOf(database));
        Context context = getApplicationContext();
        toastText = "Data berhasil disimpan.";
        Toast toast = Toast.makeText(context, toastText, toastDuration);
        toast.show();
        finish();
    }
}